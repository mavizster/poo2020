import java.time.LocalDate;

public class Persona {

	private String nombre, genero, email, contraseña;
	private LocalDate fechaNacimiento;
	
	
	public Persona(String nombre, String genero, String email, LocalDate fechaNacimiento) {
		super();
		this.setNombre(nombre);
		this.setGenero(genero);
		this.setEmail(email);
		this.setFechaNacimiento (fechaNacimiento);
		this.setContraseña("1234");
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getGenero() {
		return genero;
	}


	public void setGenero(String genero) {
		this.genero = genero;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getContraseña() {
		return contraseña;
	}


	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}


	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}


	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}


	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", genero=" + genero + ", email=" + email + ", contraseña=" + contraseña
				+ ", fechaNacimiento=" + fechaNacimiento + "]";
	}
	
	
}
